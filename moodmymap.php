<html lang="es">
  <head>
  <meta http-equiv="Expires" content="0">
  <meta http-equiv="Last-Modified" content="0">
  <meta http-equiv="Cache-Control" content="no-cache"/>
  <meta http-equiv="Pragma" content="no-cache">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Modificar diagrama arq</title>
  <style>
      button,input[type=submit],input[type=reset]
    {
      background-color: #D6EAF8;
      padding: 4px 4px;
      border: outset #ABB2B9;
      cursor: pointer;
      font-size: 15px;
      font-weight: bold;
      box-shadow: 2px 3px 10px #000033;
    }
    .evilbtn
    {
      display: inline;
      background-color: #F7DC6F;
      border:  double #FCF3CF;
      font-size: 15px;
      font-weight: bold;
      float: right;
      margin-right: 5%;
      cursor: default;
      border-radius: 50%;
      height: 60px;
    }
    .container
    {
      padding: 4px 4px;
      box-sizing: border-box;
      font-size: 14px;
      display: inline-block;
      position: relative;
      left: 10px;
    }
    form { display: inline; }
    .DaBox
    {
        width: 50%;
      margin-right:30px;
      margin-left:30px;
      border:5px groove #102b3d;
      border-radius: 25px;
      text-shadow: 1px 1px 5px #5DADE2;
      #background: #d0cbd6;
    }
    .unselectable
    {
      -webkit-touch-callout: none;
      -webkit-user-select: none;
      -khtml-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }
  </style>
   <?php
  include 'dbc.php';
  $conn = mysqli_connect($host,$user,$pass,$db);
  if($_SERVER['REQUEST_METHOD']=="POST"&&isset($_POST['thatimage']))
  {
    $fl=$_POST['imag'];
    $isupp=0;
    $add="";
    $nimg = $_FILES['RR']['name'];
    $timg = $_FILES['RR']['type'];
    if ($nimg == !NULL)
    {
      $r= explode( '.', $fl ) ;
      if(($timg == "image/jpeg") || ($timg == "image/jpg") || ($timg == "image/png"))
      {
        if($timg == "image/jpeg")
        {
          $add="all-of-those-images/fil/".$r[0].".jpeg";
          $timg="jpeg";
        }
        if($timg == "image/jpg")
        {
          $add="all-of-those-images/fil/".$r[0].".jpg";
          $timg="jpg";
        }
        if($timg == "image/png")
        {
          $add="all-of-those-images/fil/".$r[0].".png";
          $timg="png";
        }
        $k=$r[0].".".$timg;
        $fl="";
      }
      else
      {
        $faceless = $_FILES['RR']['tmp_name'];
        if ($_FILES['RR']['error'] !== 0)
          echo 'Error al subir el archivo ';
        else
        {
          if (mime_content_type($_FILES['RR']['tmp_name']) == 'application/pdf')
          {
            $add="all-of-those-images/fil/".$r[0].".pdf";
            $timg="pdf";
            $k=$r[0].".".$timg;
            $fl="";
          }
          else
          {
            $fl="Formato no permitido";
            $isup=99;
          }
        }
      }
    }
    else
    {
      $fl="Ninguna imagen detectada en Diagrama de arquitectura";
      $isup=99;
    }
    if($isup==99)
      echo '<script type="text/javascript">alert("'.$fl.'");</script>';
    else
    {
      $fl="all-of-those-images/fil/".$_POST['imag'];
      unlink($fl);
      $sql="update filtro set filtro.DA='".$k."' where filtro.FR2=".$_POST['no'];
      $re2=mysqli_query($conn,$sql);
      if(!$re)
        echo "Conexion con BD fallida".mysqli_error();
      move_uploaded_file($_FILES["RR"]["tmp_name"],$add);
      echo  $_FILES["RR"]["tmp_name"].$add;
      echo "<form method=\"post\" action=\"moodmymap.php\" id=\"jumplikeaboss\" name=\"jumplikeaboss\"><input type=\"hidden\" name=\"no\" id=\"no\" value=\"".$_POST['no']."\">";
      if ($_POST['no']>0)
        echo "<script type=\"text/javascript\">document.getElementById('jumplikeaboss').submit();</script></form>";
    }
  }
  ?>
  </head>
  <body>
  <?php
    $sql = "select DA  from filtro where FR2=".$_POST['no'];
    $re = mysqli_query($conn,$sql);
    if(!$re)
      echo "Conexion con BD fallida".mysqli_error();
    else
    {
      echo "<form class=\"container\" method=\"POST\" action=\"".htmlspecialchars($_SERVER['PHP_SELF'])."\" enctype=\"multipart/form-data\"><div class=\"menu\">Diagrama de arquitectura :<br><input type=\"file\" name=\"RR\" id=\"RR\"><button type=\"submit\"  name='thatimage' id='thatimage' >Remplazar mapa</button></div>";
      $row = mysqli_fetch_array($re);
      $v=$row['DA'];
      echo "<input type=\"hidden\" name=\"no\" id=\"no\" value=\"".$_POST['no']."\">";
      echo "<input type=\"hidden\" name=\"imag\" id=\"imag\" value=\"".$v."\">";
      $fechaSegundos = time();
      $strNoCache = "?nocache=$fechaSegundos";
      $r=explode('.',$v);
      if($r[1]=="pdf")
        echo "<iframe src=\"all-of-those-images/fil/".$v."\" width=\"100%\" height=\"80%\" </iframe></form>";
      else
        echo "<br><img style=\"width:90%;max-height:80%;\" src='all-of-those-images/fil/".$v.$strNoCache."' ></form>";
    }
    mysqli_close($conn);
  ?>
  </body>
</html>